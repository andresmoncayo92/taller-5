const inputs = document.querySelectorAll('#form div input'),
    correo = document.getElementById('email'),
    adver = document.getElementById('warning');

const expresionesRegulares = {
    numeros : /([0-9])/,
    text : /([a-zA-Z])/,
    caracteres : /[^a-zA-Z\d\s]/,
    correo : /([a-z\d]+[@]+[a-z]+\.[a-z]{2,})/,
    espacios : /\s/g
}

inputs.forEach(input=>{
    input.addEventListener('keyup', (e) => { 
        let valueInput = e.target.value;

        switch(e.target.id){
            case 'user':
                input.value = valueInput.replace(expresionesRegulares.espacios, '').replace(expresionesRegulares.caracteres, '');
            break;
            case 'email':
                if(valueInput == ''){
                    adver.innerHTML = '';
                }else{
                    if(expresionesRegulares.correo.test(e.target.value)){
                        correo.style.border = 'none';
                    }else{
                        correo.style.border = '2px solid #ce1212';
                        adver.innerHTML = 'El correo que ingreso no es correcto';
                    }
                }
            break;
            case 'password':
                input.value = valueInput.replace(expresionesRegulares.espacios, '');

                if(e.target.value.length < 8){
                    adver.innerHTML = 'Solo se permiten contraseñas mayor a 8 caracteres';
                }else{
                    adver.innerHTML = '';
                }
            break;
        }
    })
});


form.addEventListener("submit", e=>{
    e.preventDefault();
    let warnings ="";
    let entrar  = false;

    inputs.forEach(e=>{
        if(e.value == ''){
            warnings = 'Todos los campos son requeridos';
            entrar = false;
            return;
        }else{
            entrar = true;
        }
    });

    if(entrar == false){
        adver.innerHTML = warnings;
    }else{
        adver.innerHTML = "Enviado";
    }

    form.reset();
})